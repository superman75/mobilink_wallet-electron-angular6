import { HomeComponent } from './components/home/home.component';
import { ExchangeComponent } from './components/exchange/exchange.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WalletComponent } from './components/wallet/wallet.component';
import { BackupComponent } from './components/backup/backup.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'exchange',
        component: ExchangeComponent
    },
    {
        path: 'wallet',
        component: WalletComponent
    },
    {
        path: 'backup',
        component: BackupComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
